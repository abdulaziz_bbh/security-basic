package com.example.amigoscodejwtauthentication.controller;

import com.example.amigoscodejwtauthentication.dto.*;
import com.example.amigoscodejwtauthentication.service.AuthenticationService;
import com.example.amigoscodejwtauthentication.service.OtpService;
import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;
    private final OtpService otpService;

    @PostMapping("register")
    public ResponseEntity<UserResponse> register(
            @RequestBody RegisterRequest request
    ) {
        return ResponseEntity.ok(authenticationService.register(request));
    }

    @PostMapping("authenticate")
    public ResponseEntity<Boolean> login(
            @RequestBody AuthenticationRequest request
    ) {
        return ResponseEntity.ok(authenticationService.authenticate(request));
    }

    @PostMapping("verify")
    public ResponseEntity<AuthenticationResponse> verifyOtp(@RequestBody VerifyTokenRequestDto verifyTokenRequestDto) {
        return otpService.verifyOtp(verifyTokenRequestDto);
    }
}
