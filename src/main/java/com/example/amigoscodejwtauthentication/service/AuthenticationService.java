package com.example.amigoscodejwtauthentication.service;

import com.example.amigoscodejwtauthentication.config.JwtService;
import com.example.amigoscodejwtauthentication.domain.Role;
import com.example.amigoscodejwtauthentication.domain.User;
import com.example.amigoscodejwtauthentication.dto.AuthenticationRequest;
import com.example.amigoscodejwtauthentication.dto.RegisterRequest;
import com.example.amigoscodejwtauthentication.dto.UserResponse;
import com.example.amigoscodejwtauthentication.repository.UserRepository;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {


    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final OtpService otpService;


    public UserResponse register(RegisterRequest request) {


        var user = User.builder()
                .firstName(request.getFirtName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.USER)
                .build();
        userRepository.save(user);
        return UserResponse.builder()
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
    }
    public Boolean authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        var user = userRepository.findByEmail(request.getEmail())
                .orElseThrow();
        return otpService.generateOtp(user.getEmail());
    }
}
