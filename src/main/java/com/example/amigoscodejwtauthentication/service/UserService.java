package com.example.amigoscodejwtauthentication.service;

import com.example.amigoscodejwtauthentication.domain.User;
import com.example.amigoscodejwtauthentication.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public List<User> findAllUser(){
        return this.userRepository.findAll();
    }

    public String findEmailByUsername(String username){
        Optional<User> user = userRepository.findByEmail(username);
        if (user.isPresent()){
            return user.get().getEmail();
        }
        throw new UsernameNotFoundException("User Not found");
    }
}
