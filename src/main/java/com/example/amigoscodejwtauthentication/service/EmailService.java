package com.example.amigoscodejwtauthentication.service;

import com.example.amigoscodejwtauthentication.dto.EmailDto;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EmailService {

    private final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    private final JavaMailSender javaMailSender;

    public Boolean sendSimpleMessage(EmailDto emailDto){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(String.join(",", emailDto.getRecipients()));
        mailMessage.setSubject(emailDto.getSubject());
        mailMessage.setText(emailDto.getBody());

        Boolean isSent = false;

        try {
            javaMailSender.send(mailMessage);
            isSent = Boolean.TRUE;
        }catch (Exception e){
            LOGGER.error("Seding email error {}", e.getMessage());
        }
        return isSent;
    }
}
