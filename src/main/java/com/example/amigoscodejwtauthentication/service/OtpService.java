package com.example.amigoscodejwtauthentication.service;

import com.example.amigoscodejwtauthentication.config.JwtService;
import com.example.amigoscodejwtauthentication.dto.AuthenticationResponse;
import com.example.amigoscodejwtauthentication.dto.EmailDto;
import com.example.amigoscodejwtauthentication.dto.VerifyTokenRequestDto;
import com.example.amigoscodejwtauthentication.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OtpService {

    private final Logger LOGGER  = LoggerFactory.getLogger(OtpService.class);

    private final EmailService emailService;
    private final UserService userService;
    private final UserRepository userRepository;
    private final OtpGenerator otpGenerator;
    private final JwtService jwtService;

    public Boolean generateOtp(String key) {

        Integer otpValue = otpGenerator.generateOTP(key);
        if (otpValue == -1){
            LOGGER.error("OTP generator is not working...");
            return  false;
        }
        LOGGER.info("Generated otp: {}", otpValue);

        String userEmail = userService.findEmailByUsername(key);
        List<String> recipients = new ArrayList<>();
        recipients.add(userEmail);

        EmailDto emailDto = new EmailDto();
        emailDto.setSubject("Spring Boot OTP Password.");
        emailDto.setBody("OTP password: "+otpValue);
        emailDto.setRecipients(recipients);

        return emailService.sendSimpleMessage(emailDto);
    }
    public Boolean validateOTP(String key, Integer otpNumber){
        Integer cacheOTP = otpGenerator.getOTPByKey(key);
        if (cacheOTP!=null && cacheOTP.equals(otpNumber)){
            otpGenerator.clearOTPFromCache(key);
            return true;
        }
        return false;
    }
    public ResponseEntity<AuthenticationResponse> verifyOtp(VerifyTokenRequestDto verifyTokenRequestDto){
        String username = verifyTokenRequestDto.getUsername();
        Integer otp = verifyTokenRequestDto.getOtp();
        Boolean rememberMe = verifyTokenRequestDto.getRememberMe();

        boolean isOtpValid = validateOTP(username, otp);
        if (!isOtpValid){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        var user = userRepository.findByEmail(verifyTokenRequestDto.getUsername())
                .orElseThrow();
        var jwtToken  = jwtService.generateToken(user);
       var auth =  AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
        return new ResponseEntity<>(auth, HttpStatus.OK);
    }

}
