package com.example.amigoscodejwtauthentication.domain;

public enum Role {

    ADMIN,
    USER
}
