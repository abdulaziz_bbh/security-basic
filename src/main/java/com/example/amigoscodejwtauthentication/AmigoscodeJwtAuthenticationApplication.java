package com.example.amigoscodejwtauthentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmigoscodeJwtAuthenticationApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmigoscodeJwtAuthenticationApplication.class, args);
    }

}
