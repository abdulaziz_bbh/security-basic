package com.example.amigoscodejwtauthentication.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UserResponse {

    private String email;

    private String firstName;

    private String lastName;
}
