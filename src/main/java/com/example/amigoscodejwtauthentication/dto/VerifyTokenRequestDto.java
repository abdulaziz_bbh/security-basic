package com.example.amigoscodejwtauthentication.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VerifyTokenRequestDto {


    private String username;

    private Integer otp;

    private Boolean rememberMe;
}
